package id.bts.alfatest.module.detail;

import java.util.Map;

import id.bts.alfatest.model.detail.DetailMovieResponse;
import id.bts.alfatest.model.review.MovieReviewResponse;
import id.bts.alfatest.model.video.MovieVideoResponse;
import id.bts.alfatest.service.ApiVersion;
import id.bts.alfatest.service.BaseApi;
import id.bts.alfatest.service.endpoint.ApiEndpoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

class DetailModel {

    private static final String TAG = DetailModel.class.getSimpleName();

    private final DetailPresenter presenter;

    DetailModel(DetailPresenter presenter) {
        this.presenter = presenter;
    }

    public void getMovieDetail(int id, Map<String, String> query) {
        BaseApi.getInstance()
                .beginRequest(ApiVersion.VERSION_3, false)
                .create(ApiEndpoint.class)
                .getMovieDetail(id, query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<DetailMovieResponse>>() {
                    @Override
                    public void onNext(Response<DetailMovieResponse> result) {
                        if (result.isSuccessful()) {
                            DetailMovieResponse response = result.body();
                            presenter.onFetchDetailSuccess(response);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }

    public void fetchMovieReviews(int id, Map<String, String> query) {
        BaseApi.getInstance()
                .beginRequest(ApiVersion.VERSION_3, false)
                .create(ApiEndpoint.class)
                .getMovieReviews(id, query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<MovieReviewResponse>>() {
                    @Override
                    public void onNext(Response<MovieReviewResponse> result) {
                        if (result.isSuccessful()) {
                            MovieReviewResponse response = result.body();
                            presenter.onFetchReviewSuccess(response);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }

    public void fetchMovieVideos(int id, Map<String, String> query) {
        BaseApi.getInstance()
                .beginRequest(ApiVersion.VERSION_3, false)
                .create(ApiEndpoint.class)
                .getMovieVideos(id, query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<MovieVideoResponse>>() {
                    @Override
                    public void onNext(Response<MovieVideoResponse> result) {
                        if (result.isSuccessful()) {
                            MovieVideoResponse response = result.body();
                            presenter.onFetchVideoSuccess(response);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }
}
