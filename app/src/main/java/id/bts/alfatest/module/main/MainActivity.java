package id.bts.alfatest.module.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bts.alfatest.R;
import id.bts.alfatest.helper.GridPaginationHelper;
import id.bts.alfatest.model.discover.DiscoverMovieItem;
import id.bts.alfatest.model.discover.DiscoverResponse;
import id.bts.alfatest.module.detail.DetailActivity;
import id.bts.alfatest.module.main.adapter.DiscoverMoviesAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private MainPresenter presenter;

    @BindView(R.id.discover_recycler_view)
    RecyclerView rvDiscover;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    private int mCurrentPage = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int pageSize = 20;
    private DiscoverMoviesAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.presenter = new MainPresenter(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupView();
    }

    private void setupView() {
        setupRecyclerView();
        toolbarTitle.setText("Discover");
        presenter.fetchDiscoverList(mCurrentPage);
    }

    private void setupRecyclerView() {
        GridLayoutManager gm = new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false);
        rvDiscover.setLayoutManager(gm);
        rvDiscover.setHasFixedSize(true);
        rvDiscover.setNestedScrollingEnabled(false);
        rvDiscover.addOnScrollListener(new GridPaginationHelper(gm) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;

                presenter.fetchDiscoverList(mCurrentPage);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        mAdapter = new DiscoverMoviesAdapter();
        mAdapter.setOnItemClickListener(item -> presenter.openMovieDetail(item));
        rvDiscover.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public void showData(@NotNull DiscoverResponse data) {
        mCurrentPage = data.getPage();

        int totalPages = data.getTotalPages();
        List<DiscoverMovieItem> items = data.getResults();

        if (data.getResults().size() != 0) {
            if (mCurrentPage == 1) {
                mAdapter.addAll(items);

                if (mCurrentPage < totalPages) mAdapter.addLoadingFooter();
                else isLastPage = true;
            } else {
                mAdapter.removeLoadingFooter();
                isLoading = false;
                mAdapter.addAll(items);

                if (mCurrentPage != totalPages) mAdapter.addLoadingFooter();
                else isLastPage = true;
            }
        }
    }

    private void showError(Throwable error) {

    }

    private void showUnsuccessful(int code) {

    }
}
