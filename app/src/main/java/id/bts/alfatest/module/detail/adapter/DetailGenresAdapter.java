package id.bts.alfatest.module.detail.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bts.alfatest.R;
import id.bts.alfatest.model.detail.DetailMovieGenres;

public class DetailGenresAdapter extends RecyclerView.Adapter<DetailGenresAdapter.DetailCategoryViewHolder> {

    private List<DetailMovieGenres> data = new ArrayList<>();

    @NonNull
    @Override
    public DetailCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chip_genres, parent, false);
        return new DetailCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailCategoryViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void add(DetailMovieGenres item) {
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }

    public void addAll(@NotNull List<DetailMovieGenres> items) {
        for (DetailMovieGenres item : items) {
            add(item);
        }
    }

    public void remove(DetailMovieGenres item) {
        int position = data.indexOf(item);
        if (position > -1) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    public DetailMovieGenres getItem(int position) {
        return data.get(position);
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public static class DetailCategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.detail_genres_chip)
        Chip chipGenres;

        public DetailCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(@NotNull DetailMovieGenres item) {
            chipGenres.setText(item.getName());
        }
    }
}
