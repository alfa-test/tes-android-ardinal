package id.bts.alfatest.module.detail;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bts.alfatest.R;
import id.bts.alfatest.model.detail.DetailMovieGenres;
import id.bts.alfatest.model.detail.DetailMovieResponse;
import id.bts.alfatest.model.review.MovieReviewItem;
import id.bts.alfatest.model.video.MovieVideoItem;
import id.bts.alfatest.module.detail.adapter.DetailGenresAdapter;
import id.bts.alfatest.module.detail.adapter.DetailReviewAdapter;
import id.bts.alfatest.module.detail.adapter.DetailTrailerAdapter;

public class DetailActivity extends AppCompatActivity {

    private static final String TAG = DetailActivity.class.getSimpleName();
    public static final String EXTRAS_KEY = TAG + "EXTRAS";

    private DetailPresenter presenter;
    private int movieId = -1;

    @BindView(R.id.detail_backdrop_image)
    ImageView imgBackdrop;
    @BindView(R.id.detail_poster_image)
    ImageView imgPoster;
    @BindView(R.id.detail_summary_title)
    TextView tvTitle;
    @BindView(R.id.detail_summary_star)
    ImageView ratingStar;
    @BindView(R.id.detail_summary_vote_count)
    TextView tvVoteCount;
    @BindView(R.id.detail_summary_genres_rv)
    RecyclerView rvGenres;
    @BindView(R.id.detail_trailer_rv)
    RecyclerView rvTrailer;
    @BindView(R.id.detail_overview_text)
    TextView tvOverview;
    @BindView(R.id.detail_review_rv)
    RecyclerView rvReview;
    @BindView(R.id.toolbar_back_button)
    ImageButton toolbarBack;

    private DetailGenresAdapter mGenresAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.presenter = new DetailPresenter(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setupView();
    }

    private void setupView() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            movieId = extras.getInt(EXTRAS_KEY);
        }

        toolbarBack.setOnClickListener(v -> onBackPressed());

        setupGenresRecycler();
        setupTrailerRecycler();
        setupReviewRecycler();

        presenter.fetchMovieDetail(movieId);
    }

    private void setupGenresRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvGenres.setLayoutManager(layoutManager);
        rvGenres.setNestedScrollingEnabled(false);
        mGenresAdapter = new DetailGenresAdapter();
        rvGenres.setAdapter(mGenresAdapter);
        mGenresAdapter.notifyDataSetChanged();
    }

    private void setupTrailerRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvTrailer.setLayoutManager(layoutManager);
        rvTrailer.setNestedScrollingEnabled(false);
    }

    private void setupReviewRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvReview.setLayoutManager(layoutManager);
        rvReview.setNestedScrollingEnabled(false);
    }

    public void showBackdropImage(String imageUrl) {
        Glide.with(this)
                .load(imageUrl)
                .into(imgBackdrop);
    }

    public void showPosterImage(String imageUrl) {
        Glide.with(this)
                .load(imageUrl)
                .into(imgPoster);
    }

    public void showSummaryData(@NotNull DetailMovieResponse data) {
        tvTitle.setText(data.getOriginalTitle());
        tvVoteCount.setText(String.valueOf(data.getVoteAverage()));

        List<DetailMovieGenres> genres = data.getGenres();
        if (genres != null && genres.size() > 0) {
            mGenresAdapter.addAll(genres);
        }

        tvOverview.setText(data.getOverview());
    }

    public void showTrailer(List<MovieVideoItem> data) {
        DetailTrailerAdapter mTrailerAdapter = new DetailTrailerAdapter(data, this.getLifecycle());
        rvTrailer.setAdapter(mTrailerAdapter);
        mTrailerAdapter.notifyDataSetChanged();
    }

    public void showReview(List<MovieReviewItem> data) {
        DetailReviewAdapter mReviewAdapter = new DetailReviewAdapter(data);
        rvReview.setAdapter(mReviewAdapter);
        mReviewAdapter.notifyDataSetChanged();
    }
}
