package id.bts.alfatest.module.main;

import android.content.Intent;
import android.os.Bundle;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import id.bts.alfatest.BuildConfig;
import id.bts.alfatest.model.discover.DiscoverMovieItem;
import id.bts.alfatest.model.discover.DiscoverResponse;
import id.bts.alfatest.module.detail.DetailActivity;
import okhttp3.ResponseBody;

class MainPresenter {

    private static final String TAG = MainPresenter.class.getSimpleName();

    private final MainActivity view;
    private final MainModel model;

    MainPresenter(MainActivity view) {
        this.view = view;
        this.model = new MainModel(this);
    }

    public void fetchDiscoverList(Integer page) {
        Map<String, String> query = new HashMap<>();
        query.put("api_key", BuildConfig.TMDB_API_KEY);
        query.put("page", page.toString());
        model.getDiscoverList(query);
    }

    public void onGetDiscoverListSuccess(DiscoverResponse response) {
        view.showData(response);
    }

    public void onGetDiscoverListFailed(int code, ResponseBody responseBody) {
//        view.showOnFailedFetchData();
    }

    public void onGetDiscoverListError(Throwable e) {
        if (e instanceof ConnectException) {
//            view.showConnectExceptionWarning();
        } else if (e instanceof SocketTimeoutException) {
//            view.showConnectionTimeoutWarning();
        } else {
//            view.showGeneralError();
        }
    }

    public void openMovieDetail(DiscoverMovieItem item) {
        Intent intent = new Intent(view, DetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(DetailActivity.EXTRAS_KEY, item.getId());
        intent.putExtras(bundle);
        view.startActivity(intent);
    }
}
