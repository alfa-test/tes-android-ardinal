package id.bts.alfatest.module.detail;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import id.bts.alfatest.BuildConfig;
import id.bts.alfatest.model.detail.DetailMovieResponse;
import id.bts.alfatest.model.review.MovieReviewResponse;
import id.bts.alfatest.model.video.MovieVideoResponse;

class DetailPresenter {

    private static final String TAG = DetailPresenter.class.getSimpleName();

    private final DetailActivity view;
    private final DetailModel model;

    private int movieId;
    private Map<String, String> queryParams;


    DetailPresenter(DetailActivity view) {
        this.view = view;
        this.model = new DetailModel(this);

        initQueryParams();
    }

    private void initQueryParams() {
        Map<String, String> query = new HashMap<>();
        query.put("api_key", BuildConfig.TMDB_API_KEY);
        query.put("page", "1");
        this.queryParams = query;
    }

    public void fetchMovieDetail(int movieId) {
        this.movieId = movieId;
        model.getMovieDetail(movieId, queryParams);
    }

    private String getImageUrl(String path) {
        return BuildConfig.TMDB_IMAGE_URL + "original" + path;
    }

    public void onFetchDetailSuccess(DetailMovieResponse response) {
        model.fetchMovieVideos(movieId, queryParams);

        if (response.getBackdropPath() != null) {
            view.showBackdropImage(getImageUrl(response.getBackdropPath()));
        }

        if (response.getPosterPath() != null) {
            view.showPosterImage(getImageUrl(response.getPosterPath()));
        }

        view.showSummaryData(response);
    }

    public void onFetchVideoSuccess(MovieVideoResponse response) {
        Log.d(TAG, "onFetchVideoSuccess: " + response);
        model.fetchMovieReviews(movieId, queryParams);

        if (response.getResults() != null && response.getResults().size() > 0) {
            view.showTrailer(response.getResults());
        }
    }

    public void onFetchReviewSuccess(MovieReviewResponse response) {
        Log.d(TAG, "onFetchReviewSuccess: " + response);
        if (response.getResults() != null && response.getResults().size() > 0) {
            view.showReview(response.getResults());
        }
    }
}
