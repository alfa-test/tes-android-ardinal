package id.bts.alfatest.module.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerDrawable;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bts.alfatest.BuildConfig;
import id.bts.alfatest.R;
import id.bts.alfatest.model.discover.DiscoverMovieItem;

public class DiscoverMoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<DiscoverMovieItem> data = new ArrayList<>();
    private DiscoverItemClickListener clickListener;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_movies_main, parent, false);
        viewHolder = new ItemViewHolder(itemView);
        return viewHolder;
    }

    public void add(DiscoverMovieItem item) {
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }

    public void addAll(@NotNull List<DiscoverMovieItem> items) {
        for (DiscoverMovieItem item : items) {
            add(item);
        }
    }

    public void remove(DiscoverMovieItem item) {
        int position = data.indexOf(item);
        if (position > -1) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    public DiscoverMovieItem getItem(int position) {
        return data.get(position);
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void addLoadingFooter() {
        for (int i = 0; i < 2; i++) {
            add(new DiscoverMovieItem());
        }
    }

    public void removeLoadingFooter() {
        for (int i = 0; i < 2; i++) {
            int position = data.size() - 1;
            DiscoverMovieItem wod = getItem(position);
            if (wod != null) {
                data.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.bindData(data.get(position), clickListener);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnItemClickListener(DiscoverItemClickListener listener) {
        this.clickListener = listener;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_item_image)
        ImageView imgCover;
        @BindView(R.id.movie_item_title)
        TextView tvTitle;
        @BindView(R.id.movie_item_vote)
        TextView tvVote;
        @BindView(R.id.movie_item_date)
        TextView tvDate;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(@NotNull DiscoverMovieItem item, DiscoverItemClickListener clickListener) {
            String imageUrl = null;
            if (item.getPosterPath() != null) {
                imageUrl = BuildConfig.TMDB_IMAGE_URL + "original" + item.getPosterPath();
            }

            showImage(imageUrl);
            tvTitle.setText(item.getTitle());
            tvVote.setText(String.valueOf(item.getVoteAverage()));

            if (item.getReleaseDate() != null) {
                tvDate.setText(getDisplayedDate(item.getReleaseDate()));
            } else {
                tvDate.setText(null);
            }

            itemView.setOnClickListener(v -> clickListener.onItemClicked(item));
        }

        private String getDisplayedDate(String date) {
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            try {
                Date old = dt.parse(date);
                SimpleDateFormat dt1 = new SimpleDateFormat("dd MMM yy", Locale.getDefault());
                return old != null ? dt1.format(old) : date;
            } catch (ParseException ex) {
                return date;
            }
        }

        private void showImage(String url) {
            Shimmer shimmer = new Shimmer.AlphaHighlightBuilder()
                    .setDuration(1800)
                    .setBaseAlpha(0.7f)
                    .setFixedHeight(500)
                    .setHighlightAlpha(0.6f)
                    .setDirection(Shimmer.Direction.LEFT_TO_RIGHT)
                    .setAutoStart(true)
                    .build();

            ShimmerDrawable shimmerDrawable = new ShimmerDrawable();
            shimmerDrawable.setShimmer(shimmer);

            if (url == null) {
                Glide
                        .with(itemView.getContext())
                        .load(shimmerDrawable)
                        .into(imgCover);
            } else {
                Glide
                        .with(itemView.getContext())
                        .load(url)
                        .placeholder(shimmerDrawable)
                        .into(imgCover);
            }
        }
    }

    public interface DiscoverItemClickListener {
        void onItemClicked(DiscoverMovieItem item);
    }
}
