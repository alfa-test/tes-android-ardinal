package id.bts.alfatest.module.detail.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerDrawable;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bts.alfatest.R;
import id.bts.alfatest.model.video.MovieVideoItem;

public class DetailTrailerAdapter extends RecyclerView.Adapter<DetailTrailerAdapter.DetailTrailerViewHolder> {

    private List<MovieVideoItem> data;
    private Lifecycle lifecycle;

    public DetailTrailerAdapter(List<MovieVideoItem> data, Lifecycle lifecycle) {
        this.data = data;
        this.lifecycle = lifecycle;
    }

    @NonNull
    @Override
    public DetailTrailerAdapter.DetailTrailerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        YouTubePlayerView view = (YouTubePlayerView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_trailer, parent, false);
        lifecycle.addObserver(view);
        return new DetailTrailerAdapter.DetailTrailerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailTrailerAdapter.DetailTrailerViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class DetailTrailerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.trailer_video_player)
        YouTubePlayerView playerView;

        private YouTubePlayer mPlayer;
        private String videoKey;

        public DetailTrailerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            playerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(@NotNull YouTubePlayer youTubePlayer) {
                    mPlayer = youTubePlayer;
                    mPlayer.cueVideo(videoKey, 0);
                }
            });
        }

        void bindData(@NotNull MovieVideoItem item) {
            videoKey = item.getKey();
            if (mPlayer == null) return;

            mPlayer.cueVideo(videoKey, 0);
        }
    }
}
