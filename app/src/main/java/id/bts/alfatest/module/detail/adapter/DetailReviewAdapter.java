package id.bts.alfatest.module.detail.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bts.alfatest.R;
import id.bts.alfatest.model.review.MovieReviewItem;

public class DetailReviewAdapter extends RecyclerView.Adapter<DetailReviewAdapter.DetailReviewViewHolder> {

    protected List<MovieReviewItem> data;
    private Lifecycle lifecycle;

    public DetailReviewAdapter(List<MovieReviewItem> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public DetailReviewAdapter.DetailReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movies_review, parent, false);
        return new DetailReviewAdapter.DetailReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailReviewAdapter.DetailReviewViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class DetailReviewViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.review_author_name)
        TextView tvName;
        @BindView(R.id.review_author_content)
        TextView tvContent;
        @BindView(R.id.review_author_divider)
        LinearLayout divider;

        public DetailReviewViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(@NotNull MovieReviewItem item) {
            tvName.setText(item.getAuthor());
            tvContent.setText(item.getContent());

            if (item == data.get(data.size() - 1)) {
                divider.setVisibility(View.GONE);
            }
        }
    }
}
