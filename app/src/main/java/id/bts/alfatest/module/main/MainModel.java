package id.bts.alfatest.module.main;

import java.util.Map;

import id.bts.alfatest.model.discover.DiscoverResponse;
import id.bts.alfatest.service.ApiVersion;
import id.bts.alfatest.service.BaseApi;
import id.bts.alfatest.service.endpoint.ApiEndpoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

class MainModel {

    private static final String TAG = MainModel.class.getSimpleName();

    private final MainPresenter presenter;

    MainModel(MainPresenter presenter) {
        this.presenter = presenter;
    }

    public void getDiscoverList(Map<String, String> page) {
        BaseApi.getInstance()
                .beginRequest(ApiVersion.VERSION_3, false)
                .create(ApiEndpoint.class)
                .getDiscoverMovie(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<DiscoverResponse>>() {
                    @Override
                    public void onNext(Response<DiscoverResponse> result) {
                        if (result.isSuccessful()) {
                            DiscoverResponse response = result.body();
                            if (response != null) {
                                presenter.onGetDiscoverListSuccess(response);
                            }
                        } else {
                            presenter.onGetDiscoverListFailed(result.code(), result.errorBody());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        presenter.onGetDiscoverListError(e);
                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }
}
