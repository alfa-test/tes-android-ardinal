package id.bts.alfatest.service.endpoint;

import java.util.Map;

import id.bts.alfatest.model.detail.DetailMovieResponse;
import id.bts.alfatest.model.discover.DiscoverResponse;
import id.bts.alfatest.model.review.MovieReviewResponse;
import id.bts.alfatest.model.video.MovieVideoResponse;
import io.reactivex.Observable;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface ApiEndpoint {
    @GET("discover/movie")
    Observable<Response<DiscoverResponse>> getDiscoverMovie(@QueryMap Map<String, String> query);

    @GET("movie/{movie_id}")
    Observable<Response<DetailMovieResponse>> getMovieDetail(@Path("movie_id") int movieId, @QueryMap Map<String, String> query);

    @GET("movie/{movie_id}/reviews")
    Observable<Response<MovieReviewResponse>> getMovieReviews(@Path("movie_id") int movieId, @QueryMap Map<String, String> query);

    @GET("movie/{movie_id}/videos")
    Observable<Response<MovieVideoResponse>> getMovieVideos(@Path("movie_id") int movieId, @QueryMap Map<String, String> query);
}
