package id.bts.alfatest.service;

public enum ApiVersion {
    VERSION_3(3),
    VERSION_4(4);

    public final int version;

    ApiVersion(int version) {
        this.version = version;
    }
}
