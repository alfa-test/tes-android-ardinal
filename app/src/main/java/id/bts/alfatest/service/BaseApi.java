package id.bts.alfatest.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import id.bts.alfatest.BuildConfig;
import id.bts.alfatest.helper.PreferenceHelper;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApi {

    private static final String BASE_URL = BuildConfig.BASE_URL;
    private static volatile BaseApi INSTANCE;

    private BaseApi() {
        if (INSTANCE != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    private static final String TAG = BaseApi.class.getSimpleName();

    public static BaseApi getInstance() {
        if (INSTANCE == null) {
            synchronized (BaseApi.class) {
                if (INSTANCE == null) {
                    INSTANCE = new BaseApi();
                }
            }
        }
        return INSTANCE;
    }

    @NotNull
    public Retrofit beginRequest(ApiVersion version, boolean withAuth) {
        OkHttpClient.Builder httpClient = buildHeader(withAuth);
        String endpoint = BASE_URL + version.version + "/";
        Retrofit.Builder retrofit = buildRetrofit(httpClient.build(), endpoint);
        return retrofit.build();
    }

    @NotNull
    private OkHttpClient.Builder buildHeader(boolean withAuth) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder reqBuilder = original.newBuilder();
            if (withAuth)
                reqBuilder.addHeader("Authorization", "Bearer " + PreferenceHelper.getInstance().getApiToken());
            reqBuilder.method(original.method(), original.body());
            Request request = reqBuilder.build();
            return chain.proceed(request);
        });

        builder
                .addInterceptor(getLoggingInterceptor())
                .callTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        return builder;
    }

    @NotNull
    private HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;

        interceptor.level(level);
        return interceptor;
    }

    @NotNull
    private Retrofit.Builder buildRetrofit(OkHttpClient client, String url) {
        try {
            Retrofit.Builder builder = new Retrofit.Builder();
            builder
                    .client(client)
                    .baseUrl(url)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .addConverterFactory(GsonConverterFactory.create());

            return builder;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }
}
