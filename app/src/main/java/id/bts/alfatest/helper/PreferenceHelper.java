package id.bts.alfatest.helper;

import id.bts.alfatest.BuildConfig;

public class PreferenceHelper {

    private static volatile PreferenceHelper INSTANCE;

    private PreferenceHelper() {
        if (INSTANCE != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static PreferenceHelper getInstance() {
        if (INSTANCE == null) {
            synchronized (PreferenceHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new PreferenceHelper();
                }
            }
        }

        return INSTANCE;
    }

    public String getApiKey() {
        return BuildConfig.TMDB_API_KEY;
    }

    public String getApiToken() {
        return BuildConfig.TMDB_API_TOKEN;
    }
}
