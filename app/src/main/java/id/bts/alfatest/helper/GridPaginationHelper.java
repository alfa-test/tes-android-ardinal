package id.bts.alfatest.helper;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public abstract class GridPaginationHelper extends RecyclerView.OnScrollListener {

    private static final String TAG = GridPaginationHelper.class.getSimpleName();

    private final GridLayoutManager layoutManager;

    protected GridPaginationHelper(@NotNull GridLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (!isLoading() && !isLastPage()) {
            int visibleItemCount = layoutManager.findLastCompletelyVisibleItemPosition() + 1;
            if (visibleItemCount == layoutManager.getItemCount()) {
                loadMoreItems();
            }
        }
    }

    protected abstract void loadMoreItems();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();
}

