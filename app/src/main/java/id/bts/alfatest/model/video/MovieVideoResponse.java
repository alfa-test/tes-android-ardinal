package id.bts.alfatest.model.video;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MovieVideoResponse {

    @SerializedName("id")
    private int id;

    @SerializedName("results")
    private List<MovieVideoItem> results;

    public int getId() {
        return id;
    }

    public List<MovieVideoItem> getResults() {
        return results;
    }
}