package id.bts.alfatest.model.review;

import com.google.gson.annotations.SerializedName;

public class MovieReviewItem {

	@SerializedName("author")
	private String author;

	@SerializedName("id")
	private String id;

	@SerializedName("content")
	private String content;

	@SerializedName("url")
	private String url;

	public String getAuthor(){
		return author;
	}

	public String getId(){
		return id;
	}

	public String getContent(){
		return content;
	}

	public String getUrl(){
		return url;
	}
}