package id.bts.alfatest.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class RequestTokenResponse {
    @SerializedName("status_message")
    private String statusMessage;

    @SerializedName("status_code")
    private int statusCode;

    @SerializedName("success")
    private boolean success;

    @SerializedName("request_token")
    private String requestToken;

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getRequestToken() {
        return requestToken;
    }

    @NotNull
    @Override
    public String toString() {
        return
                "Response{" +
                        "status_message = '" + statusMessage + '\'' +
                        ",status_code = '" + statusCode + '\'' +
                        ",success = '" + success + '\'' +
                        ",request_token = '" + requestToken + '\'' +
                        "}";
    }
}
