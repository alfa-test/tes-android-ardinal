package id.bts.alfatest.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BaseModel<T> {
    @Nullable
    private final T data;
    private final int unsuccessfulCode;
    @Nullable
    private final Throwable error;

    private BaseModel(@Nullable T data, @Nullable Throwable error, int unsuccessfulCode) {
        this.data = data;
        this.error = error;
        this.unsuccessfulCode = unsuccessfulCode;
    }

    public static <T> BaseModel<T> success(@NonNull T data) {
        return new BaseModel<>(data, null, -1);
    }

    public static <T> BaseModel<T> error(@NonNull Throwable error) {
        return new BaseModel<>(null, error, -1);
    }

    public static <T> BaseModel<T> unsuccessful(int unsuccessfulCode) {
        return new BaseModel<>(null, null, unsuccessfulCode);
    }

    @Nullable
    public T getData() {
        return data;
    }

    public int getUnsuccessful() {
        return unsuccessfulCode;
    }

    @Nullable
    public Throwable getError() {
        return error;
    }
}
