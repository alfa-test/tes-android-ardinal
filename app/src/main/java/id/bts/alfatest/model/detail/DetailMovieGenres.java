package id.bts.alfatest.model.detail;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class DetailMovieGenres {

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @NotNull
	@Override
    public String toString() {
        return "GenresItem{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}