// Generated code from Butter Knife. Do not modify!
package id.bts.alfatest.module.main.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import id.bts.alfatest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DiscoverMoviesAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private DiscoverMoviesAdapter.ItemViewHolder target;

  @UiThread
  public DiscoverMoviesAdapter$ItemViewHolder_ViewBinding(
      DiscoverMoviesAdapter.ItemViewHolder target, View source) {
    this.target = target;

    target.imgCover = Utils.findRequiredViewAsType(source, R.id.movie_item_image, "field 'imgCover'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.movie_item_title, "field 'tvTitle'", TextView.class);
    target.tvVote = Utils.findRequiredViewAsType(source, R.id.movie_item_vote, "field 'tvVote'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.movie_item_date, "field 'tvDate'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DiscoverMoviesAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgCover = null;
    target.tvTitle = null;
    target.tvVote = null;
    target.tvDate = null;
  }
}
