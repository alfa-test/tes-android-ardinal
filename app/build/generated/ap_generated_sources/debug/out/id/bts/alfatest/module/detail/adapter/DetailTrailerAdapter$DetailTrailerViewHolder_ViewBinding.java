// Generated code from Butter Knife. Do not modify!
package id.bts.alfatest.module.detail.adapter;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import id.bts.alfatest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailTrailerAdapter$DetailTrailerViewHolder_ViewBinding implements Unbinder {
  private DetailTrailerAdapter.DetailTrailerViewHolder target;

  @UiThread
  public DetailTrailerAdapter$DetailTrailerViewHolder_ViewBinding(
      DetailTrailerAdapter.DetailTrailerViewHolder target, View source) {
    this.target = target;

    target.playerView = Utils.findRequiredViewAsType(source, R.id.trailer_video_player, "field 'playerView'", YouTubePlayerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailTrailerAdapter.DetailTrailerViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.playerView = null;
  }
}
