// Generated code from Butter Knife. Do not modify!
package id.bts.alfatest.module.detail.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import id.bts.alfatest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailReviewAdapter$DetailReviewViewHolder_ViewBinding implements Unbinder {
  private DetailReviewAdapter.DetailReviewViewHolder target;

  @UiThread
  public DetailReviewAdapter$DetailReviewViewHolder_ViewBinding(
      DetailReviewAdapter.DetailReviewViewHolder target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.review_author_name, "field 'tvName'", TextView.class);
    target.tvContent = Utils.findRequiredViewAsType(source, R.id.review_author_content, "field 'tvContent'", TextView.class);
    target.divider = Utils.findRequiredViewAsType(source, R.id.review_author_divider, "field 'divider'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailReviewAdapter.DetailReviewViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.tvContent = null;
    target.divider = null;
  }
}
