// Generated code from Butter Knife. Do not modify!
package id.bts.alfatest.module.detail.adapter;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.chip.Chip;
import id.bts.alfatest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailGenresAdapter$DetailCategoryViewHolder_ViewBinding implements Unbinder {
  private DetailGenresAdapter.DetailCategoryViewHolder target;

  @UiThread
  public DetailGenresAdapter$DetailCategoryViewHolder_ViewBinding(
      DetailGenresAdapter.DetailCategoryViewHolder target, View source) {
    this.target = target;

    target.chipGenres = Utils.findRequiredViewAsType(source, R.id.detail_genres_chip, "field 'chipGenres'", Chip.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailGenresAdapter.DetailCategoryViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.chipGenres = null;
  }
}
