// Generated code from Butter Knife. Do not modify!
package id.bts.alfatest.module.detail;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import id.bts.alfatest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailActivity_ViewBinding implements Unbinder {
  private DetailActivity target;

  @UiThread
  public DetailActivity_ViewBinding(DetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DetailActivity_ViewBinding(DetailActivity target, View source) {
    this.target = target;

    target.imgBackdrop = Utils.findRequiredViewAsType(source, R.id.detail_backdrop_image, "field 'imgBackdrop'", ImageView.class);
    target.imgPoster = Utils.findRequiredViewAsType(source, R.id.detail_poster_image, "field 'imgPoster'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.detail_summary_title, "field 'tvTitle'", TextView.class);
    target.ratingStar = Utils.findRequiredViewAsType(source, R.id.detail_summary_star, "field 'ratingStar'", ImageView.class);
    target.tvVoteCount = Utils.findRequiredViewAsType(source, R.id.detail_summary_vote_count, "field 'tvVoteCount'", TextView.class);
    target.rvGenres = Utils.findRequiredViewAsType(source, R.id.detail_summary_genres_rv, "field 'rvGenres'", RecyclerView.class);
    target.rvTrailer = Utils.findRequiredViewAsType(source, R.id.detail_trailer_rv, "field 'rvTrailer'", RecyclerView.class);
    target.tvOverview = Utils.findRequiredViewAsType(source, R.id.detail_overview_text, "field 'tvOverview'", TextView.class);
    target.rvReview = Utils.findRequiredViewAsType(source, R.id.detail_review_rv, "field 'rvReview'", RecyclerView.class);
    target.toolbarBack = Utils.findRequiredViewAsType(source, R.id.toolbar_back_button, "field 'toolbarBack'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgBackdrop = null;
    target.imgPoster = null;
    target.tvTitle = null;
    target.ratingStar = null;
    target.tvVoteCount = null;
    target.rvGenres = null;
    target.rvTrailer = null;
    target.tvOverview = null;
    target.rvReview = null;
    target.toolbarBack = null;
  }
}
