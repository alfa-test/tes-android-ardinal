/**
 * Automatically generated file. DO NOT MODIFY
 */
package id.bts.alfatest;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "id.bts.alfatest";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: debug
  public static final String BASE_URL = "https://api.themoviedb.org/";
  public static final String TMDB_API_KEY = "2913a104562fe84acf9e73a787b28214";
  public static final String TMDB_API_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyOTEzYTEwNDU2MmZlODRhY2Y5ZTczYTc4N2IyODIxNCIsInN1YiI6IjVlYjUxZDU2MGNiMzM1MDAyMGNhYmNhYiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.rSbsEMm70tJ5-fxGi14PTW6AfA-xdiOhtjS2cWJDI5I";
  public static final String TMDB_IMAGE_URL = "https://image.tmdb.org/t/p/";
}
